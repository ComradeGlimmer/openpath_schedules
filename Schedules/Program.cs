﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

using Schedules.OpenPath;

namespace Schedules
{
    class Program
    {
        private static readonly string OpenPathEmail;
        private static readonly string OpenPathPassword;


        static Program()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();

            OpenPathEmail = configuration[nameof(OpenPathEmail)];
            OpenPathPassword = configuration[nameof(OpenPathPassword)];
        }


        static async Task Main(string[] args)
        {
            Console.WriteLine("Start");

            var schedulesService = new SchedulesService(2198, OpenPathEmail, OpenPathPassword);

            var scheduleBlocks = await schedulesService.GetAllScheduleBlocks(9185);

            var block = scheduleBlocks.ToList()[0];

            await schedulesService.CreateScheduleBlock(9185, block);

            Console.WriteLine("End");
            Console.ReadKey();
        }
    }
}
