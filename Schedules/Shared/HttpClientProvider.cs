﻿using System.Net.Http;

namespace Schedules
{
    public static class HttpClientProvider
    {
        public static readonly HttpClient HttpClient = new HttpClient();
    }
}
