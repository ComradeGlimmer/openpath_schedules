﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Schedules.Shared
{
    public class ApiService
    {
        private string _authToken = string.Empty;

        private readonly HttpClient _httpClient;
        private string _email;
        private string _password;


        public ApiService(HttpClient httpClient, string email, string password)
        {
            _httpClient = httpClient;
            _email = email;
            _password = password;
        }


        public async Task<HttpResponseMessage> SendRequestAsync(string url, HttpMethod method, object body = null)
        {
            var requestMessage = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", _authToken);
            if (body != null)
            {
                var serializedBody = JsonConvert.SerializeObject(body);
                var jsonStringContent = new StringContent(serializedBody, Encoding.UTF8, "application/json");

                requestMessage.Content = jsonStringContent;
            }

            var responseMessage = await _httpClient.SendAsync(requestMessage);
            if (responseMessage.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                await SetAuthorizationToken();

                return await SendRequestAsync(url, method, body);
            }

            return responseMessage;
        }


        private async Task SetAuthorizationToken()
        {
            const string authEndpoint = "https://api.openpath.com/auth/login";

            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(authEndpoint),
            };

            var body = new
            {
                email = _email,
                password = _password
            };

            var serializedBody = JsonConvert.SerializeObject(body);
            var jsonStringContent = new StringContent(serializedBody, Encoding.UTF8, "application/json");
            requestMessage.Content = jsonStringContent;

            var result = await _httpClient.SendAsync(requestMessage);
            var content = await result.Content.ReadAsStringAsync();
            var deserializedContent = JsonConvert.DeserializeObject<AuthResponse>(content);

            _authToken = deserializedContent.data.token;
        }



        private class AuthResponse
        {
            public Data data { get; set; }
        }

        private class Data
        {
            public string token { get; set; }
        }
    }
}
