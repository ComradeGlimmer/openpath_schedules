﻿using System;
using System.Globalization;
using System.Text;
using iCalNET.Model;
using Schedules.Domain;

namespace Schedules.OpenPath
{
    /// <summary>
    /// Main class to convert schedule blocks to iCalText and back
    /// </summary>
    public static class ScheduleBlockHelper
    {
        private readonly static string[] DateTimeFormats;


        static ScheduleBlockHelper()
        {
            DateTimeFormats = new string[]
            {
                "yyyyMMddTHHmmss",
                "yyyyMMddTHHmmssZ"
            };
        }


        public static string ConvertToICalText(this ScheduleBlock scheduleBlock)
        {
            var builder = new StringBuilder();
            builder.Append("BEGIN:VEVENT\n");
            var effectiveFromUTC = scheduleBlock.EffectiveFromUTC.ToString("yyyyMMddTHHmmssZ");
            builder.Append($"DTSTART:{effectiveFromUTC}\n");
            var effectiveToUTC = scheduleBlock.EffectiveToUTC.ToString("yyyyMMddTHHmmssZ");
            builder.Append($"DTEND:{effectiveToUTC}\n");
            if(scheduleBlock.ScheduleBlockTypeId == (int)ScheduleBlockType.Repeating)
            {
                builder.Append("RULE:FREQ=WEEKLY;BYDAY=MO\n");
            }
            builder.Append("X-OP-ENTRY-STATE:unlocked\n");
            builder.Append("END:VEVENT");

            return builder.ToString();
        }

        public static ScheduleBlock MapToScheduleBlock(this ScheduleEvent scheduleEvent)
        {
            var vEvent = new vEvent(scheduleEvent.ICalText);

            var effectiveFromUTC = DateTime.ParseExact(vEvent.ContentLines["DTSTART"].Value, DateTimeFormats, CultureInfo.InvariantCulture);
            var effectiveToUTC = DateTime.ParseExact(vEvent.ContentLines["DTEND"].Value, DateTimeFormats, CultureInfo.InvariantCulture);
            var isRepeating = vEvent.ContentLines.ContainsKey("RRULE");

            var scheduleBlock = new ScheduleBlock
            {
                EffectiveFromUTC = effectiveFromUTC,
                EffectiveToUTC = effectiveToUTC,
                ScheduleBlockTypeId = isRepeating ? (int)ScheduleBlockType.Repeating : (int)ScheduleBlockType.OneTime
            };

            return scheduleBlock;
        }
    }
}
