﻿using Newtonsoft.Json;

namespace Schedules.OpenPath
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ScheduleEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("iCalText")]
        public string ICalText { get; set; }
    }
}
