﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using iCalNET.Model;
using Newtonsoft.Json;
using Schedules.Domain;
using Schedules.Shared;

namespace Schedules.OpenPath
{
    public class SchedulesService : ISchedulesService
    {
        private const string ApiAddress = "https://api.openpath.com";

        private readonly int _orgId;

        private readonly ApiService _apiService;


        public SchedulesService(int orgId, string openPathEmail, string openPathPassword)
        {
            _orgId = orgId;

            _apiService = new ApiService(HttpClientProvider.HttpClient, openPathEmail, openPathPassword);
        }


        public async Task<IReadOnlyCollection<ScheduleBlock>> GetAllScheduleBlocks(int scheduleId)
        {
            var url = $"{ApiAddress}/orgs/{_orgId}/schedules/{scheduleId}/events";

            var responseMessage = await _apiService.SendRequestAsync(url, HttpMethod.Get);
            var json = await responseMessage.Content.ReadAsStringAsync();
            var parsedResponse = JsonConvert.DeserializeObject<ScheduleEventsResponse>(json);

            var scheduleBlocks = parsedResponse.Data.Select(e => e.MapToScheduleBlock()).ToList();

            return scheduleBlocks;
        }

        public async Task<bool> CreateScheduleBlock(int scheduleId, ScheduleBlock scheduleBlock)
        {
            var url = $"{ApiAddress}/orgs/{_orgId}/schedules/{scheduleId}/events";
            var requestBody = new CreateScheduleEventRequestBody
            {
                ICalText = scheduleBlock.ConvertToICalText()
            };

            var responseMessage = await _apiService.SendRequestAsync(url, HttpMethod.Post, requestBody);

            return responseMessage.StatusCode == System.Net.HttpStatusCode.Created;
        }



        private class ScheduleEventsResponse
        {
            [JsonProperty("data")]
            public ICollection<ScheduleEvent> Data { get; set; }
        }

        [JsonObject(MemberSerialization.OptIn)]
        public class CreateScheduleEventRequestBody
        {
            [JsonProperty("iCalText")]
            public string ICalText { get; set; }
        }
    }
}
