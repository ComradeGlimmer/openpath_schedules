﻿namespace Schedules.Domain
{
    public enum ScheduleBlockType
    {
        Repeating = 1,
        OneTime = 2
    }
}
