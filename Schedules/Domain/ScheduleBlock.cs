﻿using System;

namespace Schedules.Domain
{
    public class ScheduleBlock
    {
        public int Id { get; set; }

        public DateTime EffectiveFromUTC { get; set; }

        public DateTime EffectiveToUTC { get; set; }

        public int ScheduleBlockTypeId { get; set; }

        public int? DayOfWeek { get; set; }
    }
}
